﻿# -*- coding: utf-8 -*-
# Support ===TON TEAM BOTT===
from linepy import *
from akad.ttypes import *
import time, asyncio, json, threading, codecs, sys, os, re, urllib, requests, wikipedia, html5lib, timeit, pafy, youtube_dl, requests
from bs4 import BeautifulSoup

creator = ["ue1e7265070c2a91ae90ad98bcd4bcea9","u5246e59d14f42b0ce0a41da0dcb1c8c4","u23a0460a3446f01c8589c103d7fd9b1d","u18d2b9c606f8cf27dfee0baa460b593b"]

helpMessage ="""╔═══════════════┓
╠  ™🕸 BYMAX 🕸 
╚═══════════════┛
 ────┅═ই۝ई═┅────
             คำสั่งแอดมิน
 🌴───┅═ই۝ई═┅───🌴
╔═══════════════┓
╠❂➣T[ ]_คำสั่ง2
╠❂➣Tเพิ่มอำนาจ__@
╠❂➣Tลบอำนาจ__@
╠❂➣Tเพิ่มแอดมิน__@
╠❂➣Tลบแอดมิน__@
╠❂➣Tเพิ่มสตาป__@
╠❂➣Tลบสตาป__@
╠❂➣Tล้างดำ
╠❂➣Tเข้ากลุ่มเปิด/ปิด*
╠❂➣Tเปิด/ปิดเข้า [ออโต้]
╠❂➣Tadd_on/off
╠❂➣Tkick_@
╠❂➣Tsp
╠❂➣Tออน
╠❂➣Tออก [ออกหมด]
╠❂➣T[ ]_ออก
╠❂➣Tลบแชท
╠❂➣Tเชคบัค
╠❂➣Tป้องกันเปิด / ปิด
╠❂➣Tมุดลิ้งเปิด / ปิด
╠❂➣Tล้างอำนาจ
╠❂➣Tล้างแอดมิน
╠❂➣Tล้างสตาป
╠❂➣Tรีบอท
╠❂➣Tเชคบอท
╠❂➣Tปิง
╠❂➣Tkickall [เตะ]
╠❂➣Taddban [คท]
╠❂➣Taddban [คท]
╠❂➣T[ ]_แทค

╰═™🕸 BYMAx 🕸┛

  ✨ [ ]ใส่เลขบอท, __เว้นวรรค


"""

with open('pro.json', 'r') as fp:
    pro = json.load(fp)
with open('org.json', 'r') as fp:
    org = json.load(fp)
with open('wait2.json', 'r') as fp:
    wait2 = json.load(fp)

mulai = time.time()

def restart_program():
    python = sys.executable
    os.execl(python, python, * sys.argv)

def waktu(secs):
    mins, secs = divmod(secs,60)
    hours, mins = divmod(mins,60)
    days, hours = divmod(hours, 24)
    return '%02d วัน %02d ชั่วโมง %02d นาที %02d วินาที' % (days, hours, mins, secs)

def logError(text):
    cl.log("[ ERROR ] " + str(text))
    time_ = datetime.now()
    with open("errorLog.txt","a") as error:
        error.write("\n[%s] %s" % (str(time), text))

def cms(string, commands): #/XXX, >XXX, ;XXX, ^XXX, %XXX, $XXX...
    tex = ["+","@","/",">",";","^","%","$","?","???:","???:","????","????"]
    for texX in tex:
        for command in commands:
            if string ==command:
                return True
    return False

def sendMessage(to, text, contentMetadata={}, contentType=0):
    mes = Message()
    mes.to, mes._from = to, profile.mid
    mes.text = text
    mes.contentType, mes.contentMetadata = contentType, contentMetadata
    if to not in messageReq:
        messageReq[to] = -1
    messageReq[to] += 1

def sendMessage(self, messageObject):
        return self.Talk.client.sendMessage(0,messageObject)

class LineBot(object):
    
    def __init__(self, resp, authQR=None):
        self.resp = resp
        self.resp = self.resp+' '
        self.authQR = authQR
        self.login(authQR)
        self.fetch()
        
    def login(self, auth):
        if auth == None:
            self.client = LineClient()
        else:
            self.client = LineClient(authToken=auth)
        self.client_ch = LineChannel(self.client, channelId="1341209850")
        self.client.log("Auth Token : " + str(self.client.authToken))
        self.client.log("Channel Token : " + str(self.client_ch.channelAccessToken))
        self.mid = self.client.getProfile().mid

    def fetch(self):
        while True:
            try:
                self.operations = self.client._client.fetchOps(self.client.revision, 10, 0, 0)
                for op in self.operations:
                    if (op.type != OpType.END_OF_OPERATION):
                        self.client.revision = max(self.client.revision, op.revision)
                        self.bot(op)
            except KeyboardInterrupt:
                print('Selamat Tinggal!')
                exit()
        
    def bot(self, op):
        cl = self.client
        wait = wait2
        try:
            if op.type == 0:
                return

            if op.type == 13:
                if self.mid in op.param3:
                    if wait2["autoJoin"] == True:
                        if op.param2 not in creator and op.param2 not in org["owner"]:
                            cl.acceptGroupInvitation(op.param1)
                            time.sleep(0.00001)
                            ginfo = cl.getGroup(op.param1)
                            cl.sendMessage(op.param1,"สวัดดีคุณไม่มีอำนาจเชิญบอทนะ " + str(ginfo.name))
                            cl.sendMessage(op.param1,"ต้องการใช้บอทกรุณาติอต่อผู้\n\nhttp://line.me/ti/p/~bossbotkick")
                            cl.leaveGroup(op.param1)
                        else:
                            cl.acceptGroupInvitation(op.param1)
                            time.sleep(0.00001)
                            cl.getGrpup(op.param1)
                            if op.param3 in org["owner"] and op.param3 in creator and op.param3 in org["admin"] and op.param3 in org["staff"]:
                                if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                                    wait2["blacklist"][op.param2] = True
                                    with open('wait2.json','w',) as fp:
                                        json.dump(wait2, fp, sort_keys=True, indent=4)
                                    cl.kickoutFromGroup(op.param1,[op.param2])
                                    time.sleep(0.00001)
                                    cl.inviteIntoGroup(op.param1,[op.param3])
                                    cl.cancelGroupInvitation(op.param1,[op.param2])
                                    time.sleep(0.00001)

                if self.mid in op.param3:
                    if op.param2 in org["owner"]:
                        cl.acceptGroupInvitation(op.param1)
                        time.sleep(0.00001)

            if op.type == 13:
                if op.param2 in wait2["blacklist"]:
                    try:
                        cl.kickoutFromGroup(op.param1,[op.param2])
                        time.sleep(0.00001)
                        cl.cancelGroupInvitation(op.param1,[op.param3])
                        time.sleep(0.00001)
                    except:
                        pass

                if op.param1 in pro["Proinvite"]:
                    if op.param2 in creator or op.param2 in org["owner"] or op.param2 in org["admin"] or op.param2 in org["staff"]:
                        pass
                    else:
                        wait2["blacklist"][op.param2] = True
                        with open('wait2.json','w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        cl.kickoutFromGroup(op.param1,[op.param2])
                        group = cl.getContact(op.param3)
                        gMembMids = [contact.mid for contact in group.invitee]
                        for _mid in gMembMids:
                          if _mid in op.param3:
                            cl.cancelGroupInvitation(op.param1,[_mid])

            if op.type == 13:
                if op.param3 in wait2["blacklist"]:
                    if op.param2 in creator or op.param2 in org["owner"] or op.param2 in org["admin"] or op.param2 in org["staff"]:
                        pass
                    else:
                        wait2["blacklist"][op.param2] = True
                        with open('wait2.json','w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        cl.kickoutFromGroup(op.param1,[op.param2])
                        time.sleep(0.00001)
                        cl.cancelGroupInvitation(op.param1,[op.param3])
                        time.sleep(0.00001)

            if op.type == 19:
                if op.param1 in pro["Autokick"]:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        wait2["blacklist"][op.param2] = True
                        with open('wait2.json','w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        cl.kickoutFromGroup(op.param1,[op.param2])
                        if op.param3 not in wait2["blacklist"]:
                            cl.findAndAddContactsByMid(op.param3)
                            cl.inviteIntoGroup(op.param1,[op.param3])

                if op.param3 in creator:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        wait2["blacklist"][op.param2] = True
                        with open('wait2.json','w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        cl.acceptGroupInvitation(op.param1)
          #              time.sleep(0.00001)
                        cl.kickoutFromGroup(op.param1,[op.param2])
                   #     time.sleep(0.00001)
                        cl.inviteIntoGroup(op.param1,[op.param3])
                   #     time.sleep(0.00001)

                if op.param3 in org["owner"]:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        wait2["blacklist"][op.param2] = True
                        with open('wait2.json','w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        cl.acceptGroupInvitation(op.param1)
                        time.sleep(0.00001)
                        cl.kickoutFromGroup(op.param1,[op.param2])
                        time.sleep(0.00001)
                        cl.findAndAddContactsByMid(op.param3)
                        cl.inviteIntoGroup(op.param1,[op.param3])
                        time.sleep(0.00001)

                if op.param3 in org["admin"]:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        wait2["blacklist"][op.param2] = True
                        with open('wait2.json','w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        cl.kickoutFromGroup(op.param1,[op.param2])
                        cl.findAndAddContactsByMid(op.param3)
                        cl.inviteIntoGroup(op.param1,[op.param3])

                if op.param3 in org["staff"]:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        wait2["blacklist"][op.param2] = True
                        with open('wait2.json','w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        cl.kickoutFromGroup(op.param1,[op.param2])
                        cl.findAndAddContactsByMid(op.param3)
                        cl.inviteIntoGroup(op.param1,[op.param3])

            if op.type == 0:
                return

            if op.type == 5:
                cl.findAndAddContactsByMid(op.param1)
                if(wait2["message"]in[""," ","\n",None]):
                    pass
                else:
                    cl.sendMessage(op.param1,str(wait2["message"]))

            if op.type == 15:
                if op.param1 in wait2["bymsg"]:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        return
                    else:
                        cl.sendText(op.param1, wait2["leftmsg"])
                        print ("MEMBER HAS LEFT THE GROUP")

            if op.type == 17:
                if op.param2 in wait2["blacklist"]:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        cl.kickoutFromGroup(op.param1,[op.param2])
                        time.sleep(0.00001)

            if op.type == 32:
                if op.param1 in pro["Procancel"]:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        wait2["blacklist"][op.param2] = True
                        with open('wait2.json','w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        cl.kickoutFromGroup(op.param1,[op.param2])
                        if op.param3 not in wait2["blacklist"]:
                            cl.findAndAddContactsByMid(op.param3)
                            cl.inviteIntoGroup(op.param1,[op.param3])

                if wait2["Jscancel"] == True:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        wait2["blacklist"][op.param2] = True
                        with open('wait2.json','w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        user = cl.getContact(op.param2)
                        cl.sendMessage(op.param2,"jangan di cancel woooii.. " + str(user.displayName))
                        try:
                            if op.param3 in org["owner"]:
                                cl.findAndAddContactsByMid(op.param3)
                                cl.inviteIntoGroup(op.param1,[op.param3])
                                cl.kickoutFromGroup(op.param1,[op.param2])
                                time.sleep(0.00001)
                        except:
                            try:
                                if op.param3 in creator:
                                    cl.inviteIntoGroup(op.param1,[op.param3])
                                    cl.kickoutFromGroup(op.param1,[op.param2])
                                    time.sleep(0.00001)
                            except:
                                pass

            if op.type == 11:
                if op.param1 in pro["Proqr"]:
                    if cl.getGroup(op.param1).preventedJoinByTicket == False:
                        if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                            wait2["blacklist"][op.param2] = True
                            with open('wait2.json','w') as fp:
                                json.dump(wait2, fp, sort_keys=True, indent=4)
                            cl.reissueGroupTicket(op.param1)
                            G = cl.getGroup(op.param1)
                            G.preventedJoinByTicket = True
                            cl.updateGroup(G)
                            cl.kickoutFromGroup(op.param1,[op.param2])

                if op.param2 in wait2["blacklist"]:
                    if op.param2 not in creator and op.param2 not in org["owner"] and op.param2 not in org["admin"] and op.param2 not in org["staff"]:
                        if cl.getGroup(op.param1).preventedJoinByTicket == False:
                            cl.reissueGroupTicket(op.param1)
                            G = cl.getGroup(op.param1)
                            G.preventedJoinByTicket = True
                            cl.updateGroup(G)
                            cl.kickoutFromGroup(op.param1,[op.param2])
                            time.sleep(0.00001)

            if op.type == 11:
                if op.param1 in pro["Proname"]:
                    if op.param2 in creator or op.param2 in org["owner"] or op.param2 in org["admin"] or op.param2 in org["staff"]:
                        pass
                    else:
                        g = cl.getGroup(op.param1)
                        g.name = pro["gname"][op.param1]
                        cl.updateGroup(g)

            if op.type == 26:
                print ("[25] OP MSSG")
                msg = op.message
                text = msg.text
                msg_id = msg.id
                receiver = msg.to
                sender = msg._from
                if msg.toType == 0 and msg.toType == 2:
                    if sender != cl.profile.mid:
                        to = sender
                    else:
                        to = receiver
                else:
                    to = receiver
                if msg.contentType == 0:
                    if text is None:
                        return
                if msg.contentType == 13:
                  if msg._from in creator:
                    if wait2["addowner"] == True:
                        if msg.contentMetadata["mid"] in org["owner"]:
                            cl.sendMessage(msg.to, "was owner")
                        else:
                            org["owner"][msg.contentMetadata["mid"]] = True
                            with open('org.json', 'w') as fp:
                                json.dump(org, fp, sort_keys=True, indent=4)
                            cl.sendMessage(msg.to, "owner added")

                if msg.contentType == 13:
                  if msg._from in creator:
                    if wait2["delowner"] == True:
                        if msg.contentMetadata["mid"] in org["owner"]:
                            del org["owner"][msg.contentMetadata["mid"]]
                            with open('org.json', 'w') as fp:
                                json.dump(org, fp, sort_keys=True, indent=4)
                            cl.sendMessage(msg.to,"Owner deleted")
                        else:
                            cl.sendMessage(msg.to,"Owner not found")
#===[ Add admin ☆☆☆ ]
                if msg.contentType == 13:
                  if msg._from in creator or msg._from in org["owner"]:
                    if wait2["addadmin"]==True:
                        if msg.contentMetadata["mid"] in org["admin"]:
                            cl.sendMessage(msg.to, "was admin")
                            wait2["addadmin"]=False
                        else:
                            org["admin"][msg.contentMetadata["mid"]] = True
                            with open('org.json', 'w') as fp:
                                json.dump(org, fp, sort_keys=True, indent=4)
                            cl.sendMessage(msg.to, "admin added")
                            wait2["addadmin"]=False

                if msg.contentType == 13:
                  if msg._from in creator or msg._from in org["owner"]:
                    if wait2["deladmin"]==True:
                        if msg.contentMetadata["mid"] in org["admin"]:
                            del org["admin"][msg.contentMetadata["mid"]]
                            with open('org.json', 'w') as fp:
                                json.dump(org, fp, sort_keys=True, indent=4)
                            wait2["deladmin"]=False
                            cl.sendMessage(msg.to,"s deleted")
                        else:
                            cl.sendMessage(msg.to,"S not found")
#====[ Add staff ☆☆☆ ]
                if msg.contentType == 13:
                  if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                    if wait2["addstaff"]==True:
                        if msg.contentMetadata["mid"] in org["staff"]:
                            cl.sendMessage(msg.to, "was staff")
                            wait2["addstaff"]=False
                        else:
                            org["staff"][msg.contentMetadata["mid"]] = True
                            with open('org.json', 'w') as fp:
                                json.dump(org, fp, sort_keys=True, indent=4)
                            cl.sendMessage(msg.to, "staff added")
                            wait2["addstaff"]=False

                if msg.contentType == 13:
                  if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                    if wait2["delstaff"]==True:
                        if msg.contentMetadata["mid"] in org["staff"]:
                            del org["staff"][msg.contentMetadata["mid"]]
                            with open('org.json', 'w') as fp:
                                json.dump(org, fp, sort_keys=True, indent=4)
                            cl.sendMessage(msg.to,"staff deleted")
                            wait2["delstaff"]=False
                        else:
                            cl.sendMessage(msg.to,"Staff not found")
                            wait2["delstaff"]=False
#========[ BLACKLIST ]============#
                if msg.contentType == 13:
                  if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                    if wait2["ablacklist"]==True:
                        if msg.contentMetadata["mid"] in wait2["blacklist"]:
                            cl.sendMessage(to, "Was BL boss")
                            wait2["ablacklist"]=False
                        else:
                            wait2["blacklist"][msg.contentMetadata["mid"]] = True
                            with open('wait2.json', 'w') as fp:
                                json.dump(wait2, fp, sort_keys=True, indent=4)
                            cl.sendMessage(to, "Blacklist Saved")
                            wait2["ablacklist"]=False

                if msg.contentType == 13:
                  if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                    if wait2["dblacklist"]==True:
                        if msg.contentMetadata["mid"] in wait2["blacklist"]:
                            del wait2["blacklist"][msg.contentMetadata["mid"]]
                            with open('wait2.json', 'w') as fp:
                                json.dump(wait2, fp, sort_keys=True, indent=4)
                            cl.sendMessage(to, "Blacklist Removed")
                            wait2["dblacklist"]=False
                        else:
                            cl.sendMessage(to," target not found")
                            wait2["dblacklist"]=False
            if op.type == 26:
                print ("[26] บอทชุดทำงานอยู่")
                msg = op.message
                text = msg.text
                msg_id = msg.id
                receiver = msg.to
                sender = msg._from
                if msg.toType == 0 and msg.toType == 2:
                    if sender != cl.profile.mid:
                        to = sender
                    else:
                        to = receiver
                else:
                    to = receiver
                if msg.contentType == 7:
                    pass
                elif text is None:
                    return
                elif msg.toType == 2:
                    if msg.text == self.resp + "คำสั่ง1":
                      if msg._from in creator or msg._from in org["owner"]:
                        cl.sendMessage(msg.to,helpMessage)
                    if msg.text in ["Zkey owner"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        cl.sendMessage(msg.to,helpMessage1)
                    if msg.text in ["Zkey admin"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        cl.sendMessage(msg.to,helpMessage2)
                    elif msg.text in ["Tรีบอท"]:
                        if msg._from in creator:
                            print ("[Command]Like executed")
                            try:
                                cl.sendMessage(msg.to,"รีบอทเรียบร้อย...")
                                restart_program()
                            except:
                                cl.sendMessage(msg.to,"โปรดรอ...")
                                restart_program()
                                pass
                    elif msg.text in ["Tเชคบอท"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                          cl.sendMessage(msg.to, "บอทป้องกันกลุ่มทำงานอยู่...")
                    elif msg.text == self.resp + "คำสั่ง2":
                      if msg._from in creator or msg._from in org["owner"]:
                          md = "  🌴───คำสั่งแอดมิน───🌴   \n\n"
                          md += "❂➣" +self.resp+ "__อำนาจ [เช็ค]\n"
                          md += "❂➣" +self.resp+ "__แอดมิน [เช็ค]\n"
                          md += "❂➣" +self.resp+ "__สตาป [เช็ค]\n"
                          md += "❂➣" +self.resp+ "__เชคดำ [เช็คบชดำ]\n"
                          md += "❂➣" +self.resp+ "__kick_@\n"
                          md += "❂➣" +self.resp+ "__เปิดลิ้ง [ส่งลิ้ง]\n"
                          md += "❂➣" +self.resp+ "__ปิดลิ้ง\n"
                          md += "❂➣" +self.resp+ "__เชค [เช็คป้องกัน]\n"
                          md += "❂➣" +self.resp+ "__กลุ่ม [เที่บอทอยู่]\n"
                          md += "❂➣" +self.resp+ "__ออก [กลุ่มนี้]\n"
                          md += "✨ [ ] ใส่เลขบอท, __เว้นวรรค\n\n"
                          md += "         🌴🕸 NONAME 🕸🌴\n"                          
                          md += "  http://line.me/ti/p/~bossbotkick"
                          cl.sendMessage(msg.to,md)
                          
                          

                    elif msg.text in ["Tออน"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        eltime = time.time() - mulai
                        van = "บอททำงานมาแล้ว "+waktu(eltime)
                        cl.sendMessage(msg.to,van)
                    elif msg.text == self.resp + "เปิดลิ้ง":
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        X = cl.getGroup(msg.to)
                        if X.preventedJoinByTicket == False:
                            cl.updateGroup(X)
                            gurl = cl.reissueGroupTicket(msg.to)
                            cl.sendMessage(msg.to,"line://ti/g/" + gurl)
                        else:
                            X.preventedJoinByTicket = False
                            cl.updateGroup(X)
                            gurl = cl.reissueGroupTicket(msg.to)
                            cl.sendMessage(msg.to,"line://ti/g/" + gurl)
                    elif msg.text == self.resp + "ปิดลิ้ง":
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        X = cl.getGroup(msg.to)
                        if X.preventedJoinByTicket == True:
                            cl.sendMessage(msg.to,"ลิ้งปิดอยู่แล้ว...")
                        else:
                            X.preventedJoinByTicket = True
                            cl.updateGroup(X)
                            cl.sendMessage(msg.to,"ปิดลิ้งเรียบร้อย...")
                    elif msg.text in ["Tเชคบัค"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                         try:cl.inviteIntoGroup(to, [self.mid]);has = "Ok"  
                         except:has = "Not"
                         try:cl.kickoutFromGroup(to, [self.mid]);has1 = "Ok"
                         except:has1 = "Not"
                         if has == "Ok":sil = "【✔】ไม่บัค"
                         else:sil = "【🚫】บัค"
                         if has1 == "Ok":sil1 = "【✔】ไม่บัค"
                         else:sil1 = "【🚫】บัค"
                         cl.sendMessage(to, "✴🔥[เชคระบบบัค]🔥✴\n\n✪➣ เตะ : {}\n✪➣ เชิญ : {}".format(sil1,sil))
                    elif msg.text in ["Tปิง"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        start = time.time()
                        cl.sendMessage("u5246e59d14f42b0ce0a41da0dcb1c8c4", '.')
                        cl.sendText(msg.to,str(int(round((time.time() - start) * 1000)))+" ms") 


                    elif "Tkick " in msg.text:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        key = eval(msg.contentMetadata["MENTION"])
                        key["MENTIONEES"][0]["M"]
                        targets = []
                        for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                        for target in targets:
                            if target in org["admin"] or target in org["staff"]:
                                pass
                            else:
                                try:
                                    cl.kickoutFromGroup(msg.to,[target])
                                except:
                                    pass
                    elif self.resp + "kick " in msg.text:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        key = eval(msg.contentMetadata["MENTION"])
                        key["MENTIONEES"][0]["M"]
                        targets = []
                        for x in key["MENTIONEES"]:
                            targets.append(x["M"])
                        for target in targets:
                            if target in org["admin"] or target in org["staff"]:
                                pass
                            else:
                                try:
                                    cl.kickoutFromGroup(msg.to,[target])
                                except:
                                    pass

                    elif msg.text == self.resp + "กลุ่ม":
                        if msg._from in creator:
                            gid = cl.getGroupIdsJoined()
                            h = ""
                            for i in gid:
                                gn = cl.getGroup(i).name
                                h += " %s\n" % (gn)
                            cl.sendMessage(msg.to,"กลุ่มที่บอทอยู่\n"+ h)
                    elif self.resp + "inv to " in msg.text:
                      if msg._from in creator:
                        ng = msg.text.replace("inv to ","")
                        gid = cl.getGroupIdsJoined()
                        x = msg._from
                        for i in gid:
                                h = cl.getGroup(i).name
                                if h == ng:
                                    cl.inviteIntoGroup(i,[x])
                                    cl.sendMessage(msg.to,"Success join to ["+ h +"] group")
                                else:
                                    pass
                    elif self.resp + "leave grup " in msg.text:
                      if msg._from in creator:
                        ng = msg.text.replace("leave grup ","")
                        gid = cl.getGroupIdsJoined()
                        for i in gid:
                            h = cl.getGroup(i).name
                            if h == ng:
                                cl.sendMessage(i,"Bot di paksa keluar oleh owner!")
                                cl.leaveGroup(i)
                                cl.sendMessage(msg.to,"Success left ["+ h +"] group")
                            else:
                                pass
                    elif msg.text in ["Tout all grup"]:
                      if msg._from in creator:
                        gid = cl.getGroupIdsJoined()
                        for i in gid:
                            cl.sendMessage(i,"สนใจใช้บอทติดต่อผู้สร้างเลยค่าบ\nhttp://line.me/ti/p/~bossbotkick")
                            cl.leaveGroup(i)
                            cl.sendMessage(msg.to,"ออกทุกกลุ่มเรียบร้อย")

                    elif msg.text in ["Tkickall"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        if msg.toType == 2:
                            group = cl.getGroup(msg.to)
                            nama = [contact.mid for contact in group.members]
                            for x in nama:
                                if x not in creator:
                                    if x not in org["owner"]:
                                        if x not in org["admin"]:
                                            if x not in org["staff"]:
                                                try:
                                                    cl.kickoutFromGroup(msg.to,[x])
                                                except:
                                                    pass
                    elif msg.text in ["Tเปิดเข้า"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        wait2["autoJoin"]=True
                        cl.sendMessage(msg.to,"เปิดเข้ากลุ่มออโต้แล้ว...")
                    elif msg.text in ["Tปิดเข้า"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        wait2["autoJoin"]=False
                        cl.sendMessage(msg.to,"ปิดเข้ากลุ่มออโต้เรียบร้อย...")
                    elif msg.text in ["Tออก"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        if msg.toType == 2:
                            ginfo = cl.getGroup(msg.to)
                            try:
                                cl.leaveGroup(msg.to)
                            except:
                                pass
                    elif msg.text == self.resp + "ออก":
                      if msg._from in creator or msg._from in org["owner"]:
                        if msg.toType == 2:
                            ginfo = cl.getGroup(msg.to)
                            try:
                                cl.leaveGroup(msg.to)
                            except:
                                pass
                    elif msg.text in ["Tล้างอำนาจ"]:
                      if msg._from in creator:
                        org["owner"] = {}
                        with open('org.json', 'w') as fp:
                            json.dump(org, fp, sort_keys=True, indent=4)
                        cl.sendMessage(msg.to,"ล้างอำนาจทั้งหมดเรียบร้อย...")
                    elif msg.text == self.resp + "อำนาจ":
                      if msg._from in creator:
                        if org["owner"] == {}:
                            cl.sendMessage(msg.to,"ไม่พบผู้มีอำนาจ")
                        else:
                            mc = []
                            for mi_d in org["owner"]:
                                mc.append(mi_d)
                            pass
                            cban = cl.getContacts(mc)
                            nban = []
                            for x in range(len(cban)):
                                nban.append(cban[x].displayName)
                            pass
                            jo = "\n_ ".join(str(i) for i in nban)
                            cl.sendMessage(msg.to,"🌴===[ ผู้มีอำนาจทั้งหมด ]===🌴\n\n🔥 %s\n\n🌴===[ จำนวน: %s คน ]===🌴"%(jo,str(len(cban))))
                    elif msg.text == self.resp + "แอดมิน":
                      if msg._from in creator:
                        if org["admin"] == {}:
                            cl.sendMessage(msg.to,"ไม่พบแอดมิน")
                        else:
                            mc = []
                            for mi_d in org["admin"]:
                                mc.append(mi_d)
                            pass
                            cban = cl.getContacts(mc)
                            nban = []
                            for x in range(len(cban)):
                                nban.append(cban[x].displayName)
                            pass
                            jo = "\n_ ".join(str(i) for i in nban)
                            cl.sendMessage(msg.to,"🌴===[ แอดมินทั้งหมด ]===🌴\n\n🌪 %s\n\n🌴===[ จำนวน: %s คน ]===🌴"%(jo,str(len(cban))))
                    elif msg.text == self.resp + "สตาป":
                      if msg._from in creator:
                        if org["staff"] == {}:
                            cl.sendMessage(msg.to,"ไม่พบสตาป...")
                        else:
                            mc = []
                            for mi_d in org["staff"]:
                                mc.append(mi_d)
                            pass
                            cban = cl.getContacts(mc)
                            nban = []
                            for x in range(len(cban)):
                                nban.append(cban[x].displayName)
                            pass
                            jo = "\n_ ".join(str(i) for i in nban)
                            cl.sendMessage(msg.to,"🌴===[ สตาปทั้งหมด ]===🌴\n\n🏋 %s\n\n🌴===[ จำนวน: %s คน ]===🌴"%(jo,str(len(cban))))

                    elif msg.text in ["Tล้างแอดมิน"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        org["admin"] = {}
                        with open('org.json', 'w') as fp:
                            json.dump(org, fp, sort_keys=True, indent=4)
                        cl.sendMessage(msg.to,"ล้างแอดมินทั้งหมดเรียบร้อย...")
                    elif msg.text in ["Tล้างสตาป"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        org["staff"] = {}
                        with open('org.json', 'w') as fp:
                            json.dump(org, fp, sort_keys=True, indent=4)
                        cl.sendMessage(msg.to,"ล้างสตาปทั้งหมดเรียบร้อย...")
                    elif msg.text in ["Tautojoin on"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        wait2["autoJoin"]=True
                        cl.sendMessage(msg.to,"Auto join in Activated")
                    elif msg.text in ["Tautojoin off"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        wait2["autoJoin"]=False
                        cl.sendMessage(msg.to,"Auto join not Active")
                    elif msg.text in ["Tล้างดำ"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        wait2['blacklist'] = {}
                        with open('wait2.json', 'w') as fp:
                            json.dump(wait2, fp, sort_keys=True, indent=4)
                        cl.sendMessage(to,"°ล้างดำทั้งหมดแล้ว...")
                    elif msg.text in ["Taddban"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                            wait2["ablacklist"]=True
                            cl.sendMessage(to, "please send contact")
                    elif msg.text in ["Tdelban"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                            wait2["dblacklist"]=True
                            cl.sendMessage(to, "please send contact")

                    elif msg.text == self.resp + "เชคดำ":
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        if wait2["blacklist"] == {}:
                            cl.sendMessage(to,"ไม่พบรายชื่อคนติดดำ")
                        else:
                            mc = "_====[ คนติดดำทั้งหมด ]====_\n"
                            for mi_d in wait2["blacklist"]:
                                mc += "\n_ "+cl.getContact(mi_d).displayName
                            cl.sendMessage(msg.to,mc + "\n_====[ คนติดดำทั้งหมด ]====_")
                    elif msg.text in ["Tsp"]:
                        if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                            start = time.time()
                            cl.sendMessage("u3b07c57b6239e5216aa4c7a02687c86d", '.')
                            elapsed_time = time.time() - start
                            cl.sendMessage(msg.to, "%s วินาที่" % (elapsed_time))

                    elif msg.text in ["Tป้องกันเปิด"]:
                        if msg._from in creator or msg._from in org["owner"]:
                            pro["Proqr"][msg.to] = True
                            pro["Procancel"][msg.to] = True
                            pro["Proinvite"][msg.to] = True
                            pro["Autokick"][msg.to] = True
                            with open('pro.json','w') as fp:
                                json.dump(pro, fp, sort_keys=True, indent=4)
                            cl.sendMessage(msg.to,"เปิดป้องกันทั้งหมดแล้ว...")
                    elif msg.text in ["Tป้องกันปิด"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        if msg.to in pro["Proqr"]:
                            try:
                                del pro["Proqr"][msg.to]
                            except:
                                pass
                        if msg.to in pro["Procancel"]:
                            try:
                                del pro["Procancel"][msg.to]
                            except:
                                pass
                        if msg.to in pro["Proinvite"]:
                            try:
                                del pro["Proinvite"][msg.to]
                            except:
                                pass
                        if msg.to in pro["Autokick"]:
                            try:
                                del pro["Autokick"][msg.to]
                            except:
                                pass
                        with open('pro.json','w') as fp:
                            json.dump(pro, fp, sort_keys=True, indent=4)
                        cl.sendMessage(msg.to,"ปิดป้องกันทั้งหมดแล้ว...")
                    elif msg.text == self.resp + "เชค":
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        md = ""
                        if msg.to in pro["Proqr"]: md+="ป้องกันลิ้ง เปิด\n"
                        else: md +="ป้องกันลิ้ง ปิด\n"
                        
                        if msg.to in pro["Procancel"]: md+="ป้องกันยกเชิญ เปฺิด\n"
                        else: md+= "ป้องกันยกเชิญ ปิด\n"
                        
                        if msg.to in pro["Proinvite"]: md+="ป้องกันเชิญ เปิด\n"
                        else: md+= "ป้องกันเชิญ ปิด\n"
                        
                        if msg.to in pro["Autokick"]: md+="ป้องกันเตะ เปิด\n"
                        else:md+="ป้องกันเตะ ปิด\n"
                        
                        cl.sendMessage(msg.to,"🌴====[ เชคป้องกัน ]====🌴\n\n"+ md +"🌴====[ บอทป้องกันกลุ่ม ]====🌴")
                    elif self.resp + "gn" in msg.text:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        if msg.toType == 2:
                            X = cl.getGroup(msg.to)
                            X.name = msg.text.replace("gn","")
                            cl.updateGroup(X)

                    elif msg.text in ["Lockname on"]:
                        if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                            if msg.to in pro["Lockname"]:
                                cl.sendMessage(msg.to,"Name Group in Locked")
                            else:
                                pro["Lockname"][msg.to] = True
                                with open('pro.json','w') as fp:
                                    json.dump(pro, fp, sort_keys=True, indent=4)
                                pro["gname"][msg.to] = cl.getGroup(msg.to).name
                                with open('pro.json','w') as fp:
                                    json.dump(pro, fp, sort_keys=True, indent=4)
                                cl.sendMessage(msg.to,"Succes Locked Group Name")
                    elif msg.text in ["Lockname off"]:
                        if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                            if msg.to not in pro["Lockname"]:
                                cl.sendMessage(msg.to,"Name Group not in Locked")
                            else:
                                del pro["Lockname"][msg.to]
                                with open('pro.json','w') as fp:
                                    json.dump(pro, fp, sort_keys=True, indent=4)
                                del pro["gname"][msg.to]
                                with open('pro.json','w') as fp:
                                    json.dump(pro, fp, sort_keys=True, indent=4)
                                cl.sendMessage(msg.to,"Succes open Locked Group Name")

                    elif msg.text == self.resp +"แทค":
                        group = cl.getGroup(msg.to)
                        nama = [contact.mid for contact in group.members]
                        k = len(nama)//20
                        for a in range(k+1):
                            txt = u''
                            s=0
                            b=[]
                            for i in group.members[a*20 : (a+1)*20]:
                                b.append({"S":str(s), "E" :str(s+6), "M":i.mid})
                                s += 7
                                txt += u'@Alin \n'
                            cl.sendMessage(to, text=txt, contentMetadata={u'MENTION': json.dumps({'MENTIONEES':b})}, contentType=0)

                    elif ("Tเพิ่มอำนาจ" in msg.text):
                        if msg._from in creator:
                             key = eval(msg.contentMetadata["MENTION"])
                             key["MENTIONEES"][0]["M"]
                             targets = []
                             for x in key["MENTIONEES"]:
                                 targets.append(x["M"])
                             for target in targets:
                                 if target in org["owner"]:
                                     cl.sendMessage(msg.to,"เพิ่มอำนาจเรียบร้อย...")
                                 else:
                                     try:
                                         org["owner"][target] = True
                                         with open('org.json','w') as fp:
                                             json.dump(org, fp, sort_keys=True, indent=4)
                                         cl.sendMessage(msg.to,"เพิ่มอำนาจเรียบร้อย...")
                                     except:
                                         pass
                    elif "Tลบอำนาจ " in msg.text:
                        if msg._from in creator:
                             key = eval(msg.contentMetadata["MENTION"])
                             key["MENTIONEES"][0]["M"]
                             targets = []
                             for x in key["MENTIONEES"]:
                                 targets.append(x["M"])
                             for target in targets:
                                 if target not in org["owner"]:
                                     cl.sendMessage(msg.to,"ลบอำนาจเรียบร้อย...")
                                 else:
                                     try:
                                         del org["owner"][target]
                                         with open('org.json','w') as fp:
                                             json.dump(org, fp, sort_keys=True, indent=4)
                                         cl.sendMessage(msg.to,"ลบอำนาจเรียบร้อย...")
                                     except:
                                         pass
                    elif "Tเพิ่มแอดมิน " in msg.text:
                      if msg._from in creator or msg._from in org["owner"]:
                           key = eval(msg.contentMetadata["MENTION"])
                           key["MENTIONEES"][0]["M"]
                           targets = []
                           for x in key["MENTIONEES"]:
                               targets.append(x["M"])
                           for target in targets:
                               if target in org["admin"]:
                                   cl.sendMessage(msg.to,"เพิ่มแอดมินเรียบร้อย...")
                               else:
                                   try:
                                       org["admin"][target] = True
                                       with open('org.json','w') as fp:
                                           json.dump(org, fp, sort_keys=True, indent=4)
                                       cl.sendMessage(msg.to,"เพิ่มแอดมินเรียบร้อย...")
                                   except:
                                       pass
                    elif "Tลบแอดมิน " in msg.text:
                      if msg._from in creator or msg._from in org["owner"]:
                           key = eval(msg.contentMetadata["MENTION"])
                           key["MENTIONEES"][0]["M"]
                           targets = []
                           for x in key["MENTIONEES"]:
                               targets.append(x["M"])
                           for target in targets:
                               if target not in org["admin"]:
                                   cl.sendMessage(msg.to,"ลบแอดมินเรียบร้อย...")
                               else:
                                   try:
                                       del org["admin"][target]
                                       with open('org.json','w') as fp:
                                           json.dump(org, fp, sort_keys=True, indent=4)
                                       cl.sendMessage(msg.to,"ลบแอดมินเรียบร้อย...")
                                   except:
                                       pass
                    elif "Tเพิ่มสตาป " in msg.text:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                           key = eval(msg.contentMetadata["MENTION"])
                           key["MENTIONEES"][0]["M"]
                           targets = []
                           for x in key["MENTIONEES"]:
                               targets.append(x["M"])
                           for target in targets:
                               if target in org["staff"]:
                                   cl.sendMessage(msg.to,"เพิ่มสตาปเรียบร้อย...")
                               else:
                                   try:
                                       org["staff"][target] = True
                                       with open('org.json','w') as fp:
                                           json.dump(org, fp, sort_keys=True, indent=4)
                                       cl.sendMessage(msg.to,"เพิ่มสตาปเรียบร้อย...")
                                   except:
                                       pass
                    elif "Tลบสตาป " in msg.text:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                           key = eval(msg.contentMetadata["MENTION"])
                           key["MENTIONEES"][0]["M"]
                           targets = []
                           for x in key["MENTIONEES"]:
                               targets.append(x["M"])
                           for target in targets:
                               if target not in org["staff"]:
                                   cl.sendMessage(msg.to,"ลบสตาปเรียบร้อย...")
                               else:
                                   try:
                                       del org["staff"][target]
                                       with open('org.json','w') as fp:
                                           json.dump(org, fp, sort_keys=True, indent=4)
                                       cl.sendMessage(msg.to,"ลบสตาปเรียบร้อย...")
                                   except:
                                       pass
                    elif msg.text in ["Tลบแชท"]:
                      if msg._from in creator or msg._from in org["owner"] or msg._from in org["admin"]:
                        try:
                            cl.removeAllMessages(op.param2)
                            cl.sendMessage(to,"ลบแชททั้งหมดเรียบร้อย...")
                        except:
                            pass
                    elif msg.text in ["Tadd on"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        wait2["message"]=True
                        cl.sendMessage(to,"autoAdd on")

                    elif msg.text in ["Tadd off"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        wait2["message"]=False
                        cl.sendMessage(to,"autoAdd off")
                    elif msg.text in ["Tมุดลิ้งเปิด"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        wait2["autoJoinTicket"]=True
                        cl.sendMessage(to,"เปิดมุดลิ้งเรียบร้อย...")

                    elif msg.text in ["Tมุดลิ้งปิด"]:
                      if msg._from in creator or msg._from in org["owner"]:
                        wait2["autoJoinTicket"]=False
                        cl.sendMessage(to,"ปิดมุดลิ้งเรียบร้อย...")
                    elif "/ti/g/" in msg.text.lower():
                        if msg._from in creator or msg._from in org["owner"]:
                          if wait2["autoJoinTicket"] == True:
                             link_re = re.compile('(?:line\:\/|line\.me\/R)\/ti\/g\/([a-zA-Z0-9_-]+)?')
                             links = link_re.findall(text)
                             n_links = []
                             for l in links:
                                 if l not in n_links:
                                    n_links.append(l)
                             for ticket_id in n_links:
                                 group = cl.findGroupByTicket(ticket_id)
                                 cl.acceptGroupInvitationByTicket(group.id,ticket_id)


        except Exception as e:
            print (e)
